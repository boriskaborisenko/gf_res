<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Longman\TelegramBot\Commands\SystemCommands;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
/**
 * Generic message command
 *
 * Gets executed when any type of message is sent.
 */
class GenericmessageCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'genericmessage';
    /**
     * @var string
     */
    protected $description = 'Handle generic message';
    /**
     * @var string
     */
    protected $version = '1.1.0';
    /**
     * @var bool
     */
    protected $need_mysql = true;
    /**
     * Command execute method if MySQL is required but not available
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function executeNoDb()
    {
        // Do nothing
        return Request::emptyResponse();
    }
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
	    
	    
	    
        $message = $this->getMessage();            // Get Message object
		$chat_id = $message->getChat()->getId();   // Get the current Chat ID
		
		
		$input = trim($message->getText(true));
        $alltext = explode(" ",$input);
                
        
        
        
        //======================BOT BRAINS
        
        
        $howwords = count($alltext);
        
        
        
        if($howwords<10){
	      for($i=0;$i<$howwords;$i++){
	        $w = strtolower($alltext[$i]);
	        if($w == 'hello' || $w=='Привет' || $w=='Здрасте' || $w=='Хай' || $w=='Салют' ){
		        	$text = 'Приветяо';
					break;
	        
	        }else if($w == 'new' || $w=='product' || $w=='products'|| $w=='товарчик' || $w=='продукт' || $w=='набор' || $w=='наборы' || $w=='наборчик' || $w=='наборов' ){
		            $text = 'http://linktonewproduct — Шуруй по ссылке';
	                break;
	        
	        
	        }else if($w=='gifamin'){
		        $text = 'Oh! Gifamin';
		        break;
	        
	        }else if($w == 'как' || $w=='сам' || $w=='дела'|| $w=='чухаешь' || $w=='себя' || $w=='что' || $w=='нового'){
		            //$r = $rand(0,3);
		            //$vars = array('Я норм, как сам?','Ну, такое','ой Фсьо!');
		            //$text = $vars[$r];
	                $text = 'Нормально!';
	                break;
	        
	        }else{
		        
		        	$text = 'Не понятно :( ...';
	        	}
        	}   
        }else{
	        foreach($alltext as $oneword){
					$text = 'Ого! Дофишище словей! Я аж растерялся';
        		}
		}
        
        
  

       
        //=======================BOT BRAINS
        
        
        
        
        
        
        
        
        $data = [                                  // Set up the new message data
            'chat_id' => $chat_id,                 // Set Chat ID to send the message to
            'action' => 'typing',
            'text'    => $text,
             				   
        ];
        
        
        
        
        
        
        
        //If a conversation is busy, execute the conversation command after handling the message
        $conversation = new Conversation(
            $this->getMessage()->getFrom()->getId(),
            $this->getMessage()->getChat()->getId()
        );
        //Fetch conversation command if it exists and execute it
        if ($conversation->exists() && ($command = $conversation->getCommand())) {
            return $this->telegram->executeCommand($command);
             
        }
        return Request::sendMessage($data); 
        
        return Request::emptyResponse();
        
    }
}