<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
			<title>GIFAMINBOX</title>
			<?php include('php/static/head.php');?>
			<script src="vegas/vegas.min.js"></script>
			<link rel="stylesheet" href="vegas/vegas.min.css">
</head>
<body style="background: transparent">
 
 
 <div id="vegas" class="bp_all">
	 <div class="bp_top">
		 <div class="bp_logo"><a style="display: block; width: 100%; height: 100%;" href="http://gifamin.com"></a></div>
	 </div>
	 <div class="bp_side">
		 <div class="bp_inside">
			 <div class="bp_maintitle">Подарочная упаковка</div>
			 <div class="bp_hello"> 
				 Подарок начинается с упаковки,<br>как театр&nbsp;&nbsp;—&nbsp;&nbsp;с вешалки. 
  			</div>
			 
			 <div class="bp_block">
				 <div class="bp_block_title">Коробка</div>
				 <div class="bp_block_content">
					 <div style="margin-bottom: 10px;">Презентабельная коробка Gifamin имеет необычную форму, выполнена из дизайнерского картона усиленной прочности и декорирована яркой бумажной лентой</div>
					 <div class="bp_bc">
					 	
					 	<div style="display: inline-block; vertical-align: top">
						 <div class="bpcn">длина:</div> <div class="bpcd">36 см</div><br>
						 <div class="bpcn">ширина:</div> <div class="bpcd">24 см</div><br>	
					 	</div>
					 	
					 	<div style="display: inline-block; vertical-align: top; margin-left: 80px;">
						 <div class="bpcn">высота:</div> <div class="bpcd">12 см</div><br>
						 <div class="bpcn">толщина:</div> <div class="bpcd">&nbsp;&nbsp;2 мм</div><br>	
					 	</div>
					 	
					 </div>
					 
				</div>
			 </div>
			 
			 <div class="bp_block">
				 <div class="bp_block_title">Цвета ленты</div>
				<!--  <div class="bp_block_content">Коробка декорирована яркой бумажной лентой</div> -->
				 <div class="bp_block_content" style="font-size: 0;">
					 
					 <div class="bp_oneband">
						 <div class="bp_b bp_blue"></div>
						 <div class="bp_t">Blue</div>
					 </div>
					 
					 <div class="bp_oneband">
						 <div class="bp_b bp_red"></div>
						 <div class="bp_t">Red</div>
					 </div>
					 
					 <div class="bp_oneband">
						 <div class="bp_b bp_silver"></div>
						 <div class="bp_t">Silver</div>
					 </div>
					 
					 <div class="bp_oneband">
						 <div class="bp_b bp_brown"></div>
						 <div class="bp_t">Brown</div>
					 </div>
					 
					 <div class="bp_oneband">
						 <div class="bp_b bp_biege"></div>
						 <div class="bp_t">Biege</div>
					 </div>
				 
				 </div>
			 </div>
			 
			 <div class="bp_block">
				 <div class="bp_block_title">Наполнение</div>
				 <div class="bp_block_content">При необходимости, коробка может быть наполнена 100% экологически чистой древесной шерстью</div>
			 </div>
			 
			 <div class="bp_block">
				 <div class="bp_block_content">Что бы вы не решили спрятать внутрь, Ваш презент будет выглядеть безупречно стильно, выгодно выделяясь из сотни других. </div>
			 </div>
		 
		 </div>
	 </div>
 </div>
 
 
 <script>
$("body").vegas({
    
    
    
    slides: [
	    { src: "/vegas/img/blue.jpg" },
	    { src: "/vegas/img/grow.jpg" },
        { src: "/vegas/img/all.jpg" },
        { src: "/vegas/img/silver.jpg" },
        { src: "/vegas/img/brown.jpg" }
    ],
    cover:true,
    overlay: 'vegas/overlays/02.png',
    animation: 'random',
    transition: 'blur'
});
 </script>	
	
</body>
</html>