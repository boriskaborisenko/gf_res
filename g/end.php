<!DOCTYPE html>
<html lang="en">
<head>
	<!--
	<base href="/">
	<link rel="stylesheet" href="/css/main.min.css">
	-->
	<meta charset="UTF-8">
	<title>THE END</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, , user-scalable=no">
	<?php include('php/static/head.php');?>
</head>
<body>
	<div class="the_end">
		<div class="end_pic"><img src="images/end.png"/></div>
		<div class="end_text">
			<p>
			К сожалению мы вынуждены приостановить наш проект и переключиться только на работу с корпоративными клиентами.
			</p>
			
			<p>
			Все наши обязательства перед клиентами будут выполнены в полном объеме. 
			</p>
			
			<p>
				info@gifamin.com<br>
				<a href="tel:+380932212808" style="color:#D92A6A; text-decoration:none;">(093) 221 28 08</a>
			</p>
			</div>
	</div>
	
	<script>
		function setscreen(){
		h = $(window).height();
		w = $(window).width();
		scale = h/w;
		//console.log(scale);
		if(scale>1.32){
			$('.the_end').css({'padding-top':'18%'});
			$('.end_text').css({'padding-bottom':'30px'});
		}else{
			$('.the_end').css({'padding-top':'0'});
			$('.end_text').css({'padding-bottom':'150px'});
		}
		console.log('set screen');
		}
		
		setscreen();
		
		$( window ).resize(function() {
		setscreen();
  		});
	</script>
</body>
</html>