<?php /* Smarty version Smarty-3.0.7, created on 2018-07-26 11:52:02
         compiled from "/var/www/html/gifamin.com/megatelega//design/DEF_ME/html/register.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3464091575b5999c2c431e3-85337195%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '42b7133ac87d998ede36bdc73e1400508ce626b2' => 
    array (
      0 => '/var/www/html/gifamin.com/megatelega//design/DEF_ME/html/register.tpl',
      1 => 1531251263,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3464091575b5999c2c431e3-85337195',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/var/www/html/gifamin.com/megatelega/Smarty/libs/plugins/modifier.escape.php';
if (!is_callable('smarty_function_math')) include '/var/www/html/gifamin.com/megatelega/Smarty/libs/plugins/function.math.php';
?>

<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable("Регистрация", null, 1);?>

<h1>Регистрация</h1>

<?php if ($_smarty_tpl->getVariable('error')->value){?>
<div class="message_error">
	<?php if ($_smarty_tpl->getVariable('error')->value=='empty_name'){?>Введите имя
	<?php }elseif($_smarty_tpl->getVariable('error')->value=='empty_email'){?>Введите email
	<?php }elseif($_smarty_tpl->getVariable('error')->value=='empty_password'){?>Введите пароль
	<?php }elseif($_smarty_tpl->getVariable('error')->value=='user_exists'){?>Пользователь с таким email уже зарегистрирован
	<?php }elseif($_smarty_tpl->getVariable('error')->value=='captcha'){?>Неверно введена капча
	<?php }else{ ?><?php echo $_smarty_tpl->getVariable('error')->value;?>
<?php }?>
</div>
<?php }?>

<form class="form register_form" method="post">
	<label>Имя</label>
	<input type="text" name="name" data-format=".+" data-notice="Введите имя" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('name')->value);?>
" maxlength="255" />
	
	<label>Email</label>
	<input type="text" name="email" data-format="email" data-notice="Введите email" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('email')->value);?>
" maxlength="255" />

    <label>Пароль</label>
    <input type="password" name="password" data-format=".+" data-notice="Введите пароль" value="" />

	<div class="captcha"><img src="captcha/image.php?<?php echo smarty_function_math(array('equation'=>'rand(10,10000)'),$_smarty_tpl);?>
"/></div> 
	<input class="input_captcha" id="comment_captcha" type="text" name="captcha_code" value="" data-format="\d\d\d\d" data-notice="Введите капчу"/>

	<input type="submit" class="button" name="register" value="Зарегистрироваться">

</form>
