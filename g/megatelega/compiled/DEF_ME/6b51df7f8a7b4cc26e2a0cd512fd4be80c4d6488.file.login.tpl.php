<?php /* Smarty version Smarty-3.0.7, created on 2018-08-20 06:45:10
         compiled from "/var/www/html/gifamin.com/megatelega//design/DEF_ME/html/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4969512625b7a47561560b7-09237132%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6b51df7f8a7b4cc26e2a0cd512fd4be80c4d6488' => 
    array (
      0 => '/var/www/html/gifamin.com/megatelega//design/DEF_ME/html/login.tpl',
      1 => 1531251263,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4969512625b7a47561560b7-09237132',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/var/www/html/gifamin.com/megatelega/Smarty/libs/plugins/modifier.escape.php';
?>
<?php $_smarty_tpl->tpl_vars['meta_title'] = new Smarty_variable("Вход", null, 1);?>
   
<h1>Вход</h1>

<?php if ($_smarty_tpl->getVariable('error')->value){?>
<div class="message_error">
	<?php if ($_smarty_tpl->getVariable('error')->value=='login_incorrect'){?>Неверный логин или пароль
	<?php }elseif($_smarty_tpl->getVariable('error')->value=='user_disabled'){?>Ваш аккаунт еще не активирован.
	<?php }else{ ?><?php echo $_smarty_tpl->getVariable('error')->value;?>
<?php }?>
</div>
<?php }?>

<form class="form login_form" method="post">
	<label>Email</label>
	<input type="text" name="email" data-format="email" data-notice="Введите email" value="<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('email')->value);?>
" maxlength="255" />

    <label>Пароль (<a href="user/password_remind">напомнить</a>)</label>
    <input type="password" name="password" data-format=".+" data-notice="Введите пароль" value="" />

	<input type="submit" class="button" name="login" value="Войти">
</form>