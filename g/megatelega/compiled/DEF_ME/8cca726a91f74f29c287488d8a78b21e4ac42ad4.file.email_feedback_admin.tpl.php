<?php /* Smarty version Smarty-3.0.7, created on 2018-07-15 20:47:38
         compiled from "/var/www/html/gifamin.com/megatelega/simpla/design/html/email_feedback_admin.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1422841985b4b96ca57a640-09888257%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8cca726a91f74f29c287488d8a78b21e4ac42ad4' => 
    array (
      0 => '/var/www/html/gifamin.com/megatelega/simpla/design/html/email_feedback_admin.tpl',
      1 => 1340316080,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1422841985b4b96ca57a640-09888257',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/var/www/html/gifamin.com/megatelega/Smarty/libs/plugins/modifier.escape.php';
?><?php $_smarty_tpl->tpl_vars['subject'] = new Smarty_variable("Вопрос от пользователя ".(smarty_modifier_escape($_smarty_tpl->getVariable('feedback')->value->name)), null, 1);?>
<h1 style='font-weight:normal;font-family:arial;'>Вопрос от пользователя <?php echo smarty_modifier_escape($_smarty_tpl->getVariable('feedback')->value->name);?>
</h1>
<table cellpadding=6 cellspacing=0 style='border-collapse: collapse;'>
  <tr>
    <td style='padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;'>
      Имя
    </td>
    <td style='padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;'>
      <?php echo smarty_modifier_escape($_smarty_tpl->getVariable('feedback')->value->name);?>

    </td>
  </tr>
  <tr>
    <td style='padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;'>
      Email
    </td>
    <td style='padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;'>
      <a href='mailto:<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('feedback')->value->email);?>
?subject=<?php echo $_smarty_tpl->getVariable('settings')->value->site_name;?>
'><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('feedback')->value->email);?>
</a>
    </td>
  </tr>
  <tr>
    <td style='padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;'>
      IP
    </td>
    <td style='padding:6px; width:170; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;'>
      <?php echo smarty_modifier_escape($_smarty_tpl->getVariable('feedback')->value->ip);?>
 (<a href='http://www.ip-adress.com/ip_tracer/<?php echo $_smarty_tpl->getVariable('feedback')->value->ip;?>
/'>где это?</a>)
    </td>
  </tr>
  <tr>
    <td style='padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;'>
      Сообщение:
    </td>
    <td style='padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;'>
       <?php echo nl2br(smarty_modifier_escape($_smarty_tpl->getVariable('feedback')->value->message));?>
</a>
    </td>
  </tr>
</table>
<br><br>
Приятной работы с <a href='http://simp.la'>Simpla</a>!