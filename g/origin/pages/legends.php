<style>
	@font-face {
	font-family: "PB";
	src: url("../fonts/Proxima Nova Bold.eot");
	src: local("☺"), url("../fonts/Proxima Nova Bold.woff") format("woff"), url("../fonts/Proxima Nova Bold.ttf") format("truetype"), url("../fonts/Proxima Nova Bold.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}

@font-face {
	font-family: "PL";
	src: url("../fonts/Proxima Nova Light.eot");
	src: local("☺"), url("../fonts/Proxima Nova Light.woff") format("woff"), url("../fonts/Proxima Nova Light.ttf") format("truetype"), url("../fonts/Proxima Nova Light.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}

@font-face {
	font-family: "PR";
	src: url("../fonts/Proxima Nova Regular.eot");
	src: local("☺"), url("../fonts/Proxima Nova Regular.woff") format("woff"), url("../fonts/Proxima Nova Regular.ttf") format("truetype"), url("../fonts/Proxima Nova Regular.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}
	#title{border-radius: 4px; border:1px solid #A9A9A9; font-family: 'PB'; font-size: 20px; padding: 3px 10px; outline: none;}
	#byid{border-radius: 4px; border:1px solid #A9A9A9; font-family: 'PB'; font-size: 20px; padding: 3px 10px; outline: none; width: 80px; text-align: center;}
	
	p{margin: 0; padding:0;}
	br{margin: 0; padding: 0;}
	.genbtn{text-decoration: none; background: #2988BC; padding: 15px 40px; border-radius: 4px; color:#fff;  text-align: center; cursor:pointer;}
	.genbtn:hover{text-decoration: none; color:#fff; background: #229fff}
	
	.inl_m{display:inline-block; vertical-align: middle;}
/* Custom dropdown */
.custom-dropdown {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  margin: 0px; /* demo only */
}

.custom-dropdown select {
  background-color: #888;
  color: #fff;
  font-size: inherit;
  padding: .5em;
  padding-right: 2.5em;	
  border: 0;
  margin: 0;
  border-radius: 3px;
  text-indent: 0.01px;
  text-overflow: '';
  outline: none;
  /*Hiding the select arrow for firefox*/
  -moz-appearance: none;
  /*Hiding the select arrow for chrome*/
  -webkit-appearance:none;
  /*Hiding the select arrow default implementation*/
  appearance: none;
}
/*Hiding the select arrow for IE10*/
.custom-dropdown select::-ms-expand {
    display: none;
}

.custom-dropdown::before,
.custom-dropdown::after {
  content: "";
  position: absolute;
  pointer-events: none;
}

.custom-dropdown::after { /*  Custom dropdown arrow */
  content: "\25BC";
  height: 1em;
  font-size: .625em;
  line-height: 1;
  right: 1.2em;
  top: 50%;
  margin-top: -.5em;
}

.custom-dropdown::before { /*  Custom dropdown arrow cover */
  width: 2em;
  right: 0;
  top: 0;
  bottom: 0;
  border-radius: 0 3px 3px 0;
  background-color: rgba(0,0,0,.2);
}

.custom-dropdown::after {
  color: rgba(0,0,0,.6);
}

.custom-dropdown select[disabled] {
  color: rgba(0,0,0,.25);
}
</style>

<?php
	include_once '../includes/db_connect.php';
	echo '<h2>Legends</h2>';
	
	?>
	
	
	
<div style="width:540px;  position: relative; box-sizing: border-box;">
	
	
	
	<div style="margin-bottom: 20px;">
	<div class="inl_m" style="width: 380px;"><input type="text" style="width: 100%;" id="title" placeholder='«Title here»'></div>

	<div class="inl_m">  или введи ID <input id="byid" type="text" onkeyup="byid()" maxlength="3" placeholder="id"></div>
	</div>
	
<!-- 	<div><textarea style="font-size: 13pt; background:#fafbfc; resize: none; width: 100%; box-sizing: border-box; padding: 20px; border-radius: 4px; border-color:#eee; height: 100px;" name="" id="buffer" cols="30" rows="10"></textarea>	</div>  	 -->
	
	<div style="width: 100%;">
	<div id="summernote"></div>
	</div>
	

	
	
	<div>
		<span class="custom-dropdown">
	<select name="" id="author">
		<option value="0">Без сомелье</option>
		<option value="1">Иван Бачурин</option>
		<option value="2">Виктор Ксенофонтов</option>
		<option value="3">Платон Блызнюк</option>
		<option value="4">Александр Лавриненко</option>
		<option value="5">By Gifamin</option>
	</select>
	</span>
	</div>
	
	
	<div id="yep" style="margin-top: 50px;"><div class="genbtn" onclick="gen()">Generate Legend</div></div>
	</div>
	
<div id="output" style="display: none"></div>	