<?php
	include_once '../includes/db_connect.php';
	echo '<h2>Suppliers</h2>';
	
	
	$query = $mysqli->query("SELECT * FROM p_suppliers");
	while($row = $query->fetch_assoc()){
		$id = $row['id'];
		$prov_name = $row['name'];
		$prov_city = $row['city'];
		$c_dets = explode(',', $row['contact_details']);
		$c_dets_count = count($c_dets);
		$c_face = explode(',', $row['contact_face']);
		$c_face_count = count($c_dets);
		
		$brands = explode(',', $row['provisioner_brand']);
		$brands_count = count($brands);
		?>
		
		<div class="sup_row" id="s_row_<?php echo $id?>" onclick="supToggle(this.id)">
			<div class="sup_part_60 inl_t">
				<div class="sup_title" id="supplier_<?php echo $id?>" ><?php echo $prov_name;?></div>
			</div>
			
			<div class="sup_part_40 inl_t"></div>
			
			
			<div class="sup_details hide" id="sd_<?php echo $id?>">
				
				<div class="sbrands inl_t">
					<div class="stitle">Brands:</div>
					<?php 
						   for($i=0; $i<$brands_count; $i++){
							 if($brands[$i] == ''){
								echo 'no data'; 
							 }else{
							   echo $brands[$i].'<br>';	
							 }
							}
					?>
				</div>
				
				<div class="sphone inl_t">
					<div class="stitle">Phones:</div>
					<?php 
						   for($i=0; $i<$c_dets_count; $i++){
							 if($c_dets[$i] == ''){
								echo 'no data'; 
							 }else{
							   echo $c_dets[$i].'<br>';	
							 }
							}
					?>
				</div>
				<div class="sdata inl_t">
					<div class="stitle">Managers:</div>
					<?php 
						for($i=0; $i<$c_face_count; $i++){
							 if($c_face[$i] == ''){
								echo 'no data'; 
							 }else{
							   echo $c_face[$i].'<br>';	
							 }
							}
					?>
				</div>
				<div class="sdata inl_t">
				<div class="stitle">City:</div>
					<?php echo $prov_city;?>
				</div>
			</div>
			
		
		
		</div>
		
		
	<?php	
	}


?>


<script>
	
</script>