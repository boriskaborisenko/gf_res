<?php
	
	include('../../connect.php');
	
	
    $id = $_GET['bottle'];
    
    $query = $conn->query("SELECT * FROM apps_years WHERE id='$id' ");
	while($row = $query->fetch_assoc()){
		$name = $row['name'];
		$desc = $row['small_desc'];
		$year = $row['year'];
		$price = $row['price'];
		$image = $row['image'];
		$is_fixed = $row['is_fixed'];
		
		$end = substr($year, -1);
					
					if($end == 1){
						$word_a = 'год';
						$word_b = 'года';
					}else if($end == 2 || $end == 3 || $end == 4){
						$word_a = 'года';
						$word_b = 'года';
					}else{
						$word_a = 'лет';
						$word_b = 'года';
					}
		
	}
	
?>	


			<div class="res_pop_cont_left inl_t">
				<div id="res_pop_img" class="res_pop_img" style="background-image: url(<?php echo $image;?>);"></div>
			</div>
			<div class="res_pop_cont_right inl_t">
				
				<div class="close_bottle" onclick="closeResPop();" style="background-image: url(php/apps/years/img/bottle_cross2.svg);"></div>
				
				<div class="res_pop_title"><?php echo $name;?></div>
				<?php
				if($is_fixed == 0){
					?>
					<div class="res_pop_year">Купаж <?php echo $year.' '.$word_b;?> </div>
					<?php
				}else{
					
					
					
					
					
					?>
					<div class="res_pop_year">Выдержка <?php echo $year.' '.$word_a;?> </div>
					<?php
				}
				?>
				
				
				
				
				
				<div class="res_pop_small_desc"> 
					<?php echo $desc;?>
				</div>
				
				
				
				
				
				
				
				<div class="res_pop_price"><span class="res_pop_price_sum">5 678</span> <span> грн.</span></div>
				<div class="res_pop_price_anno">(Приблизительно)</div>
				
				
				
				<div class="res_pop_fields">
					
					<div class="res_pop_inp">
						<input class="inp" style="width: 100%; height: 36px;" type="text" id="res_pop_name" onkeyup="yearsFields();" placeholder="Ваше имя" >
					</div>
					
					<div class="res_pop_inp">
						<input class="inp" style="width: 100%; height: 36px;" type="text" id="res_pop_phone" onkeyup="yearsFields();" placeholder="Телефон" >
					</div>
					
					<div class="years_button">
						<div id="yhider" class="years_hider"></div>
						<div class="btn bt1_none" id="res_pop_go" onclick="whatPrice(<?php echo $id?>);">Уточнить цену и наличие</div>
					</div>
					
					<!-- <div class="years_button">
						<div class="btn bt_close"  style="margin-top: 20px;" onclick="closeResPop(); ">Назад</div>
					</div> -->
					
				
				</div>
				
				
				
				
			</div>
			
			
			
			<div class="res_pop_thanks off">
				<div class="res_pop_thanks_cont" id="years_complete">
					<div class="res_float">
						<div class="res_thanks_text" >Спасибо, <span id="thnx_res_name"></span>!</div>
					    <div class="res_thanks_anno">Мы перезвоним вам в течение 20 минут</div>
					</div>
				</div>
			</div>
		