<?php 
	
	include('connect.php');
	
	$app = $_GET['app'];
    $folder = $_GET['folder'];
    $path_to_apps = 'apps/';
    $img = 'php/apps/'.$folder.'/img';
    $app_build = $path_to_apps.''.$folder.'/app.php';
    $color = '#'.$_GET['color'];
    
    

?>

<div class="appcontainer" style="">
	<div class="app_bg_top" style="background-color: <?php echo $color;?>; background-image: url(<?php echo $img ?>/top_bg.png);"></div>
	<div class="app_bg_bottom" style="background-color: <?php echo $color;?>; background-image: url(<?php echo $img ?>/bottom_bg.png);"></div>
	<div class="app_main">
		<div class="app_main_cont">
			<?php include($app_build);?>
		</div>
	</div>
</div>