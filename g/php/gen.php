

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Legendmaker</title>
	<!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>

<!-- <link href="https://cdn.quilljs.com/1.2.6/quill.snow.css" rel="stylesheet">
<script src="https://cdn.quilljs.com/1.2.6/quill.min.js"></script> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script> -->
</head>

<style>
	@font-face {
	font-family: "PB";
	src: url("../fonts/Proxima Nova Bold.eot");
	src: local("☺"), url("../fonts/Proxima Nova Bold.woff") format("woff"), url("../fonts/Proxima Nova Bold.ttf") format("truetype"), url("../fonts/Proxima Nova Bold.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}

@font-face {
	font-family: "PL";
	src: url("../fonts/Proxima Nova Light.eot");
	src: local("☺"), url("../fonts/Proxima Nova Light.woff") format("woff"), url("../fonts/Proxima Nova Light.ttf") format("truetype"), url("../fonts/Proxima Nova Light.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}

@font-face {
	font-family: "PR";
	src: url("../fonts/Proxima Nova Regular.eot");
	src: local("☺"), url("../fonts/Proxima Nova Regular.woff") format("woff"), url("../fonts/Proxima Nova Regular.ttf") format("truetype"), url("../fonts/Proxima Nova Regular.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}
	#title{border-radius: 4px; border:1px solid #A9A9A9; font-family: 'PB'; font-size: 20px; padding: 3px 10px; outline: none;}
	#byid{border-radius: 4px; border:1px solid #A9A9A9; font-family: 'PB'; font-size: 20px; padding: 3px 10px; outline: none; width: 80px; text-align: center;}
	
	p{margin: 0; padding:0;}
	br{margin: 0; padding: 0;}
	.genbtn{text-decoration: none; background: #2988BC; padding: 15px 40px; border-radius: 4px; color:#fff;  text-align: center; cursor:pointer;}
	.genbtn:hover{text-decoration: none; color:#fff; background: #229fff}
	
	.inl_m{display:inline-block; vertical-align: middle;}
/* Custom dropdown */
.custom-dropdown {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  margin: 0px; /* demo only */
}

.custom-dropdown select {
  background-color: #888;
  color: #fff;
  font-size: inherit;
  padding: .5em;
  padding-right: 2.5em;	
  border: 0;
  margin: 0;
  border-radius: 3px;
  text-indent: 0.01px;
  text-overflow: '';
  outline: none;
  /*Hiding the select arrow for firefox*/
  -moz-appearance: none;
  /*Hiding the select arrow for chrome*/
  -webkit-appearance:none;
  /*Hiding the select arrow default implementation*/
  appearance: none;
}
/*Hiding the select arrow for IE10*/
.custom-dropdown select::-ms-expand {
    display: none;
}

.custom-dropdown::before,
.custom-dropdown::after {
  content: "";
  position: absolute;
  pointer-events: none;
}

.custom-dropdown::after { /*  Custom dropdown arrow */
  content: "\25BC";
  height: 1em;
  font-size: .625em;
  line-height: 1;
  right: 1.2em;
  top: 50%;
  margin-top: -.5em;
}

.custom-dropdown::before { /*  Custom dropdown arrow cover */
  width: 2em;
  right: 0;
  top: 0;
  bottom: 0;
  border-radius: 0 3px 3px 0;
  background-color: rgba(0,0,0,.2);
}

.custom-dropdown::after {
  color: rgba(0,0,0,.6);
}

.custom-dropdown select[disabled] {
  color: rgba(0,0,0,.25);
}
</style>

<body style="width: 100%; position: relative; margin: 0; padding: 0;">
	
	
	<div style="width:540px; margin: 50px auto; position: relative; box-sizing: border-box;">
	
	
	
	<div style="margin-bottom: 20px;">
	<div class="inl_m" ><input type="text" id="title" placeholder='«Title here»'></div>

	<div class="inl_m">  или введи ID <input id="byid" type="text" onkeyup="byid()" maxlength="3" placeholder="id"></div>
	</div>
	
<!-- 	<div><textarea style="font-size: 13pt; background:#fafbfc; resize: none; width: 100%; box-sizing: border-box; padding: 20px; border-radius: 4px; border-color:#eee; height: 100px;" name="" id="buffer" cols="30" rows="10"></textarea>	</div>  	 -->
	
	<div style="width: 100%;">
	<div id="summernote"></div>
	</div>
	

	
	
	<div>
		<span class="custom-dropdown">
	<select name="" id="author">
		<option value="0">Без сомелье</option>
		<option value="1">Иван Бачурин</option>
		<option value="2">Виктор Ксенофонтов</option>
		<option value="3">Платон Блызнюк</option>
		<option value="4">Александр Лавриненко</option>
		<option value="5">By Gifamin</option>
	</select>
	</span>
	</div>
	
	
	<div id="yep" style="margin-top: 50px;"><div class="genbtn" onclick="gen()">Generate Legend</div></div>
	</div>
	
<div id="output" style="display: none"></div>

<script>
	
$(document).ready(function() {
  
  $('#summernote').summernote({
	  //fontNames: ['PB', 'PR', 'PL'],
	  placeholder: 'write here...',
	  toolbar: [
		  
	  ['view', ['codeview']]
	/*
	['style', ['style']],
    ['font', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'hr']],
    ['view', ['fullscreen', 'codeview']],
    ['help', ['help']]  
	 */ 
    
  ]
  
  
  });


});

function gen(){
	title = $('#title').val();
	text =  $('.note-editable').html();
	text = text.replace(/<\/p>+/g,"");
	text = text.replace(/<p>+/g,"");
	text = text.replace(/&nbsp;+/g,"");
	text = text.replace(/<br>+/g,"*XXXXX*");
	text = text.replace(/<br \/>+/g,"*XXXXX*");
	//text = $('#buffer').val();
	//text = text.replace('&nbsp;', '');
	//newtext = $('#output').html(text);
	//newtext2 = document.getElementById('output').innerHTML;
	
	
	author = $('#author').val();
	//console.log(newtext2);
	datas = '{"title":"'+title+'","text":"'+text+'","author":"'+author+'"}';
	//datas = JSON.stringify(datas);
	//console.log(datas);
	url = '../php/genx.php?data='+datas;
	//url = 'g.php?data='+datas;
	
	/*
	var xhttp = new XMLHttpRequest();
		xhttp.open("GET",  "gen_mix.php?data="+datas, true);
		
		xhttp.send();
		xhttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
				//resp = xhttp.responseText;
				//location.href='/php/gentemp.php';
				}
		};
	*/
	
	//alert(datas);
	location.href = url;
	console.log(datas);
}


function byid(){
	id = $('#byid').val();
	if(id.length>0){
	   
	   var xhttp = new XMLHttpRequest();
		xhttp.open("GET",  "../php/genid.php?id="+id, true);
		
		xhttp.send();
		xhttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
				resp = JSON.parse(xhttp.responseText);
				//json = JSON.parse(resp);
				title_x = resp['title'];
				gettext(id);
				author_x = resp['author'];
				$('#title').val(title_x);
				if(author_x!=0){
				 $('#author').val(author_x);	
				}else{
				 $('#author').val(0);	
				}
				
				console.log(title_x);
				
				}
		};
	   	
	}else{
		$('#title').val('');
		$('#author').val(0);
		
		$('#summernote').summernote('destroy');
				$('#summernote').summernote({
					placeholder: 'write here...',
					toolbar: [
					['view', ['codeview']]
					]
				});
				
				
		console.log('wrong id');
	}
	
}

function gettext(id){
	var xhttp = new XMLHttpRequest();
		xhttp.open("GET",  "../php/genid_text.php?id="+id, true);
		
		xhttp.send();
		xhttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
				resp = xhttp.responseText;
				
				//regresp = resp.replace(/<br\s*[\/]?>/gi, "\n");
				$('#summernote').summernote('destroy');
				$('#summernote').summernote({
					placeholder: 'write here...',
					toolbar: [
					['view', ['codeview']]
					]
				});
				
				$('#summernote').summernote('code', resp);
				
					
				//resp = resp.replace('<br />', 'XXXXXXXXX');				
				//$('#buffer').val(resp);
				console.log(resp);
				
				}
		};
}

</script>
	
</body>
</html>