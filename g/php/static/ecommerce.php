<script>
	/**
 * Call this function on a product impression.
 * @param [array] productsObj
 * @param string currencyCode
 * @param string listValue
 */
function gaeec_productsImpression(productsObj, currencyCode, listValue) {
	var p_length = productsObj.length;
	var p_ids = [];
	var p_value = 0;
	for (var i = 0; i < p_length; i++) {
		productsObj[i].list = listValue;
		p_ids.push(productsObj[i].id);
		p_value += parseFloat(productsObj[i].price);
	}
	dataLayer.push({
		'event': 'productImpressions',
		'ecommerce': {
			'currencyCode': currencyCode,
			'impressions': productsObj
		},
		'dyn_content_ids': p_ids,
		'dyn_value': p_value,
		'dyn_currency': currencyCode
	});
	
	console.log('Now we call PRODUCTS IMPRESSIONS Func');
	console.log(productsObj);
	
}

/**
 * Call this function on a product view.
 * @param [array] productsObj
 * @param string currencyCode
 * @param string actionFieldKey (p.e. List)
 * @param string actionFieldValue (p.e. Search results)
 */
function gaeec_productView(productsObj, currencyCode) {
	dataLayer.push({
		'event': 'productView',
		'ecommerce': {
			'currencyCode': currencyCode,
			'detail': {
				'products': productsObj
			}
		},
    'dyn_content_ids': productsObj[0].id,
		'dyn_value': parseFloat(productsObj[0].price),
		'dyn_currency': currencyCode
	});
	
	console.log('Now we call PRODUCT VIEW Func');
	console.log(productsObj);
}

/**
 * Call this function on a product being added to cart.
 * @param [array] productsObj
 * @param string currencyCode
 * @param string actionFieldKey (p.e. List)
 * @param string actionFieldValue (p.e. Search results)
 */
function gaeec_productAddedToCart(productsObj, currencyCode) {
	var p_value = productsObj[0].quantity * parseFloat(productsObj[0].price);
	dataLayer.push({
	  'event': 'addToCart',
	  'ecommerce': {
	    'currencyCode': currencyCode,
	    'add': {
	      'products': productsObj
	    }
	  },
		'dyn_content_ids': productsObj[0].id,
		'dyn_value': p_value,
		'dyn_currency': currencyCode
	});
	console.log('Now we call ADD TO CART Func');
	console.log(productsObj);
}

/**
 * Call this function on a product being removed to cart.
 * @param [array] productsObj
 * @param string currencyCode
 * @param string actionFieldKey (p.e. List)
 * @param string actionFieldValue (p.e. Search results)
 */
function gaeec_productRemovedFromCart(productsObj, currencyCode) {
	var p_value = productsObj[0].quantity * parseFloat(productsObj[0].price);
	dataLayer.push({
	  'event': 'removeFromCart',
	  'ecommerce': {
	    'currencyCode': currencyCode,
	    'add': {
	      'products': productsObj
	    }
	  },
		'dyn_content_ids': productsObj[0].id,
		'dyn_value': p_value,
		'dyn_currency': currencyCode
	});
	
	console.log('Now we call REMOVE FROM CART Func');
	console.log(productsObj);
}

/**
 * Call this function on checkout steps.
 * @param [array] productsObj
 * @param string currencyCode
 * @param int step
 */
function gaeec_onCheckout(productsObj, currencyCode, step) {
	var p_length = productsObj.length;
	var p_ids = [];
	var p_value = 0;
	for (var i = 0; i < p_length; i++) {
		p_ids.push(productsObj[i].id);
		p_value += parseFloat(productsObj[i].price);
	}
	dataLayer.push({
		'event': 'checkout',
		'ecommerce': {
			'currencyCode': currencyCode,
			'checkout': {
				'actionField': {
			        'step': step
			      },
				'products': productsObj
			}
		},
		'dyn_content_ids': p_ids,
		'dyn_value': p_value,
		'dyn_currency': currencyCode
	});
	
	console.log('Now we call CHECKOUT Func');
	console.log(productsObj);
}

/**
 * Call this function on a product view.
 * @param [Object] orderObj
 * @param [array] productsObj
 * @param string currencyCode
 */
function gaeec_purchaseFullfilled(orderObj, productsObj, currencyCode) {
	var p_length = productsObj.length;
	var p_ids = [];
	var p_value = 0;
	for (var i = 0; i < p_length; i++) {
		p_ids.push(productsObj[i].id);
		p_value += parseFloat(productsObj[i].price);
	}
	dataLayer.push({
		'event': 'purchaseFullfilled',
		'ecommerce': {
			'currencyCode': currencyCode,
			'purchase': {
				'actionField': {
			        'id': orderObj.id,
			        'affiliation': orderObj.affiliation,
			        'revenue': orderObj.revenue,
			        'tax': orderObj.tax,
			        'shipping': orderObj.shipping
			      },
				'products': productsObj
			}
		},
		'dyn_content_ids': p_ids,
		'dyn_value': p_value,
		'dyn_currency': currencyCode
	});
	console.log('Now we call PURCHASE Func');
	console.log(productsObj);
}
</script>
	
