<?php include('php/counters.php')?>



<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title>Заказ доставка воды Днепропетровск — Питьевая вода «Васанта»</title>
	<meta name="description" itemprop="description" content="Вода в офис. Вода на дом. Доставка воды по Днепропетровску на дом и офис. Чистая питьевая вода: (056) 734-24-14">
	<meta name="keywords" itemprop="keywords" content="вода в офис, вода доставка, вода заказ, вода на дом, вода питьевая, вода с доставкой, доставка води, доставка воды, доставка воды в офис, доставка воды днепропетровск, доставка воды на дом, доставка питьевой воды, заказ воды, питьевая вода, продажа воды, чиста вода, чистая вода">
	<!-- <link rel="canonical" href="http://vasanta.com.ua/"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
	
	
	<?php include('php/fav.php');?>
	
	<link rel="stylesheet" href="css/main.min.css?v=<?php echo rand(1234,5678983)?>">
	
	<!-- <script src="js/main.js"></script> -->
</head>
<body>
	<div class="allpage">
		
		
		
		<div class="fullscr">
			<div class="fs_close" onclick="cls()"></div>
			<div class="fs_box">
				<div class="fs_title">Для заказа позвоните или отправьте сообщение</div>
				<div class="fs_phones">
			
			<div class="fs_one_phone">
			<div class="fs_p_icon mobo-vodafone-16"></div><div class="fs_num"><a class="call voda"></a></div>
			</div>
			<div class="fs_one_phone">
			<div class="fs_p_icon mobo-kyivstar-16"></div><div class="fs_num"><a class="call ks"></a></div>
			</div>
			<div class="fs_one_phone">
			<div class="fs_p_icon mobo-lifecell-16"></div><div class="fs_num"><a class="call life"></a></div>
			</div>
			<div class="fs_one_phone">
			<div class="fs_p_icon mobo-home-16"></div><div class="fs_num"><a class="call city"></a></div>
			</div>
					
				</div>
				<!-- <div class="fs_title">Мессенджеры</div> -->
				<div class="fs_mes">
					
					<div class="one_big_app b_a_t"><a title="Telegram" class="telegramlink" href="">Telegram</a></div>
					<div class="one_big_app b_a_v"><a title="Viber" class="viberlink" href="">Viber</a></div>
				</div>
			</div>
		</div>

        <div class="mobile topbar">
	        <div class="mobilenav" onclick="fscr()"></div>
	        <div class="moblogo">
		        <div class="moblogopic"></div>
		        <div class="moblogotext">васанта</div>
		     </div>
        </div>
        
        
        
        
        
		
		<div class="top_desk">
			<div class="inner" style="border-bottom: 1px solid #eee;">
				<div class="logo">
					<div class="logo_icon"></div>
					<div class="logo_text">васанта</div>
				</div>
				<div class="top_menu">
					<div class="prenav">
						<div class="linksnav inl_m">
							<!-- <div class="messengers">
									<div class="app_ viberapp inl_m"><a title="Viber" href="viber://chat?number=+380506540099"></a></div>
									<div class="app_ telegramapp inl_m"><a title="Telegram" href="tg://resolve?domain=nickname"></a></div>
							</div> -->
						</div>
						<!-- <div class="bigbtn inl_m">Заказать сейчас</div>  -->
					</div>
				</div>
				<div class="phones">
					<div class="half_phones">
						<ul>
							<li><div class="op_ mobo-lifecell-16"></div><div class="pho"><a class="call life"></a></div></li>
						 <li><div class="op_ mobo-home-16"></div><div class="pho"><a class="call city"></a></div></li>
						</ul>
					</div>
					<div class="half_phones">
						<ul>
							<li><div class="op_ mobo-kyivstar-16"></div><div class="pho"><a class="call ks"></a></div></li>
							<li><div class="op_ mobo-vodafone-16"></div><div class="pho"><a class="call voda"></a></div></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="main">
			<div class="inner">
				
				<div class="banner">
					<!-- <div class="banner_price">50</div> -->
					<div class="inbanner">
						<div class="inbanner_text">
							<div class="banner_title">Акция!</div>
							<div class="banner_info">Первая бутылка<br>бесплатно</div>
							<div class="banner_expl">Акция для новых клиентов. Залоговая стоимость тары подлежит оплате</div>
						</div>
					</div>
				</div>
				
				<div class="cards">
					
					<div class="one_card cardhome">
						<div class="card_title">Доставка воды домой</div> 
						<div class="card_nums" style="color:#76BE2E;"><?php echo $homecount;?></div>
						<div class="card_anno">клиентов доверяют нам</div>
					</div>
					
					<div class="one_card cardwork">
						<div class="card_title">Доставка в офис</div> 
						<div class="card_nums" style="color:#76BE2E;"><?php echo $officecount;?></div>
						<div class="card_anno">клиентов доверяют нам</div>
					</div>
					
				</div>
				
				
				<div class="facts">
					<h1 class="desk">О воде «Васанта»</h1>
					<div class="all_facts">
					
						<div class="one_fact">
							<div class="fact_icon ic_1"></div>
							<div class="fact_anno">Васанта имеет вкус свежей родниковой воды. Ее приятно пить и она отлично утоляет жажду</div>
						</div>
						
						<div class="one_fact">
							<div class="fact_icon ic_2"></div>
							<div class="fact_anno">Вода Васанта, обладая оптимальным уровнем минерализации (100-120 мг/л), улучшает мозговую деятельность</div>
						</div>
						
						<div class="one_fact">
							<div class="fact_icon ic_3"></div>
							<div class="fact_anno">Благодаря уникальной технологии купажирования воды сохраняется природный минеральный состав и<br>баланс минералов (Mg ,Na, Са, К)</div>
						</div>
						
						<div class="one_fact">
							<div class="fact_icon ic_4"></div>
							<div class="fact_anno">В воде Васанта стабильный уровень PH 7.4 и выше. В вашем организме сохраняется кислотно-щелочной баланс, благодаря чему Вы бодры и здоровы</div>
						</div>
						
						<div class="one_fact">
							<div class="fact_icon ic_5"></div>
							<div class="fact_anno">Система озонирования и обогащения кислородом устраняет болезнетворные бактерии. Васанта стабилизирует работу внутренних органов</div>
						</div>
						
						<div class="one_fact">
							<div class="fact_icon ic_6"></div>
							<div class="fact_anno">Васанта проходит 12 ступеней очистки восстановления, а также бесконтактный автоматизированный разлив в день доставки</div>
						</div>
						
					
					
					</div>
				</div>
				
				<div class="levels">
					<img src="images/svg/filt_prod.svg" alt="">
				</div>
				
				
				<div class="kids">
					<div class="kids_banner">
						<img class="mobile wfpic" src="images/main_graphics/kidsgif.gif" alt="">
						<h1 class="kidshead">Для взрослых и детей</h1>
						<div class="kids_text">Вода «Васанта» имеет структуру из гармоничных и упорядоченных кристаллов. Такая вода по своему составу максимально схожа с водой в теле человека. Она наполняет чистой энергией каждую клетку организма, эффективно удаляет токсины и существенно влияет на нашу повседневную жизнь.</div>
					</div>
				</div>
				
				
				
				

				
				
				<div class="prefs">
					<div class="all_prefs">
						
						<div class="pref_row">
							<div class="pref_col_1 pf_t">Показатель</div>
							<div class="pref_col_2 pf_tx">Васанта</div>
							<div class="pref_col_2 pf_t">Норма</div>
						</div>
						
						
						
						
						<div class="pref_row">
							<div class="pref_col_1">рН</div>
							<div class="pref_col_2 pf_ok">6.5-8.3</div>
							<div class="pref_col_2">6.5-8.5</div>
						</div>
						
						<div class="pref_row">
							<div class="pref_col_1">Кальций, мг/л</div>
							<div class="pref_col_2 pf_ok">5-15</div>
							<div class="pref_col_2">&lt;130</div>
						</div>
						
						<div class="pref_row">
							<div class="pref_col_1">Магний, мг/л</div>
							<div class="pref_col_2 pf_ok">2-10</div>
							<div class="pref_col_2">&lt;80</div>
						</div>
						
						<div class="pref_row">
							<div class="pref_col_1">Хлориды, мг/л</div>
							<div class="pref_col_2 pf_ok">5-40</div>
							<div class="pref_col_2">&lt;250</div>
						</div>
						
						<div class="pref_row">
							<div class="pref_col_1">Сульфаты, мг/л</div>
							<div class="pref_col_2 pf_ok">&lt;15</div>
							<div class="pref_col_2">&lt;250</div>
						</div>
						
						<div class="pref_row">
							<div class="pref_col_1">Общая щелочность, ммоль/л</div>
							<div class="pref_col_2 pf_ok">0.9-2.5</div>
							<div class="pref_col_2">&lt;6.5</div>
						</div>
						
						<div class="pref_row">
							<div class="pref_col_1">Общая жесткость, ммоль/л</div>
							<div class="pref_col_2 pf_ok">0.9-1.3</div>
							<div class="pref_col_2">&lt;7.0</div>
						</div>
						
						<div class="pref_row">
							<div class="pref_col_1">Сухой остаток, мг/л</div>
							<div class="pref_col_2 pf_ok">100-150</div>
							<div class="pref_col_2">&lt;1000</div>
						</div>
						
						
					
					
					
					
					
					</div>
				</div>
				
				<div class="certs">
					<div class="one_cert">
						<div class="c_t1">CИСТЕМА УПРАВЛІННЯ ЯКІСТЮ</div>
						<div class="c_t2">№ UA.2.166.09061-15</div>
						<div class="c_sep"></div>
						<div class="c_t1">ВІДПОВІДАЄ ВИМОГАМ</div>
						<div class="c_t3">ДСТУ ISO 9001:2009 (ISO 9001:2008)</div>
					</div>
					<div class="one_cert">
						<div class="c_t1">СИСТЕМА УПРАВЛІННЯ БЕЗПЕКОЮ</div>
						<div class="c_t2">№ UA.MQ.166-НАССР-079-15</div>
						<div class="c_sep"></div>
						<div class="c_t1">ВІДПОВІДАЄ ВИМОГАМ</div>
						<div class="c_t3">ДСТУ ISO 22000:2007 (ISO 22000:2005)</div>
					</div>
				</div>
				
				
				
				
				
				<div class="delivery">
					<h1>Доставка</h1>
					<div class="all_delivery">
						<div class="one_delivery">
							<div class="del_icon d_phone"></div>
							<div class="del_phase">Оставьте заявку<br>на воду</div>
						</div>
						<div class="del_sep"></div>
						<div class="one_delivery">
							<div class="del_icon d_car"></div>
							<div class="del_phase">Постараемся доставить<br>как можно скорее</div>
						</div>
						<div class="del_sep"></div>
						<div class="one_delivery">
							<div class="del_icon d_bottle"></div>
							<div class="del_phase">Вода у вас дома<br>или в офисе</div>
						</div>
					</div>
				</div>
				
				
				
				
			</div>
				
				
				
				
				
				<div class="del_form">
					<!-- <h1 class="mobile delhead">Доставка</h1> -->
				 <div class="indelform">
					<div class="form_part">
						<div class="part_image">
							<img src="images/svg/exch_x2.svg" alt="">
						</div>
						<div class="priceanno"> 
							Цена за одну бутылку объемом 19 литров без тары — 50 грн. 
						</div>
						
					</div>
					
					
					<div class="form_part">
						<div class="part_image">
							<img src="images/svg/exch2.svg" alt="">
						</div>
						<div class="priceanno">
							Мы принимаем на замену бутыли других компаний
						</div>
					</div>
					
				
				 </div>
				 <div class="mobile superbtn">Заказать воду</div>
				</div>
				
			
			
			
			
			
			<div class="inner">	
				
				<div class="contacts">
					<div class="inner">
						<div class="cont_box">
							
							<div class="calls_bottom desk">
								<div class="big_apps">
									<div class="one_big_app b_a_v"><a class="viberlink" title="Viber" href="">Заказ через Viber</a></div>
									<div class="one_big_app b_a_t"><a class="telegramlink" title="Telegram" href="">Заказ через Telegram</a></div>
								</div>
							</div>
							
							
							
							<div class="cont_omnibox">
								Dnipro, Дніпропетровська область<br> 
								Семипалатинський провулок 6<br><br>
								
								hello@vasanta.com.ua<br>
								<a class="city call lastnum"></a>
							</div>
							<!-- <div class="cont_part">
								+38 (050) 234-56-23<br>
								+38 (050) 234-56-23<br>
								+38 (050) 234-56-23<br>
								+38 (050) 234-56-23
							</div>
							<div class="cont_part">
								Dnipro, Дніпропетровська область, 49000<br> 
								Семипалатинський провулок 6,<br><br>
								hello@vasanta.com.ua
							</div> -->
						</div>
					</div>
				</div> 
				
				
				
			</div>	
			
		</div>
		
		
		<div class="theend">
					<div class="foot_logo"></div>
					<div class="foot_logo_text">васанта</div>
					<div class="copy">2018</div>
		</div>
		
	</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>	
<script>
			function fscr(){
		     $('.fullscr').removeClass('slideOutLeft').addClass('animated slideInLeft'); 
		     $('body').addClass('stopscroll');  
	        };
	        
	        function cls(){
		      $('.fullscr').removeClass('slideInLeft').addClass('slideOutLeft');
		      $('body').removeClass('stopscroll');  
	        }
	        
	
	viberlink = 'viber://chat?number=+380506540099';
	telegramlink = 'tg://resolve?domain=nickname';
	
	voda = '(050) 420-54-40';
	ks = '(067) 633-43-96';
	life = '(063) 687-92-99';
	city = '(056) 767-20-40';
	plink='tel:';
	
	$('.viberlink').attr('href', viberlink);
	$('.telegramlink').attr('href', telegramlink);
	
	$('.voda').html(voda).attr('href',plink+voda);
	$('.ks').html(ks).attr('href',plink+ks);
	$('.life').html(life).attr('href',plink+life);
	$('.city').html(city).attr('href',plink+city);
	
	$(document).ready(function(){
		if($(window).width()<700){
			$('.moblogo, .banner, .one_card, .form_part, .superbtn').attr('onclick','fscr()');
		}
	});
	

</script>
	
	
</body>
</html>