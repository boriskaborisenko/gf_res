
<link rel="apple-touch-icon" sizes="180x180" href="images/fav/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/fav/favicon-16x16.png">
<link rel="manifest" href="images/fav/site.webmanifest">
<link rel="mask-icon" href="images/fav/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="images/fav/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Vasanta">
<meta name="application-name" content="Vasanta">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-config" content="/images/fav/browserconfig.xml">
<meta name="theme-color" content="#ffffff">



<meta property="og:type" content="website">
<meta property="og:site_name" content="Васанта">
<meta property="og:title" content="Доставка воды">
<meta property="og:description" content="Мега-офигенная вода в Днепре. Быстро, вкусно.">
<meta property="og:url" content="http://casha.pp.ua/vas/">
<meta property="og:locale" content="ru_RU">
<meta property="og:image" content="images/main_graphics/opengraph.jpg">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">