<?php
	$base = 'http://'.$_SERVER['SERVER_NAME'].'/';
	$rand = rand(1000,12222222);
	//$ip = $_SERVER['REMOTE_ADDR'];
	
	if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    $ip = base64_encode($ip);

?>
<!DOCTYPE html>
<html lang="en" style="background:#fff;">
<head>
	<meta name="description" content="Абалденный глинтвейн. Рецепты на белом и красном вине.">
	<meta name="keywords" content="глинтвейн, купить, киев, украина, набор, глинтвейна, рецепт, все, для, офигенный, абалденный">
	<link rel="canonical" href="<?php echo $base;?>">
	<base href="<?php echo $base;?>">
	<meta charset="UTF-8">
	<title>Глинтвейн | Gifamin</title>
	<link rel="stylesheet" href="css/main.min.css?v=<?php echo $rand?>">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script src="js/js.js"></script>
	
			<meta property="og:title" content="GIFAMIN" />
			<meta property="og:url" content="<?php echo $base;?>/specials/mulled" />
			<meta property="og:image" content="<?php echo $base;?>/images/mulledpic.png" />
			<meta property="og:description" content="Глинтвейн на белом или красном вине «La Vieille Ferme»" />
			<meta property="fb:app_id" content="1060743084027750" />
	
<?php include('php/static/gtag_head.php')?>
</head>
<body style="background:#fff;">
<?php include('php/static/gtag_body.php')?>	
	
	
	<div class="winepop off">
		<div class="winepopbox off">
			<div class="closewine" onclick="closewinepop()"></div>
			<div class="winepopcontent">
				<div class="inwinecontent_ok"><div>Спасибо за заказ!</div><div class="wine_thanks">Скоро мы свяжемся с вами</div></div>
				<div class="inwinecontent">
					<!-- <div class="wine_order_icon"><img src="images/wine/hero.png" alt="" class="imgresp"></div> -->
					<div class="wine_order_title">Ваш заказ</div>
					<div class="wine_order_consist">
						<div class="wine_order_item inl_m winefont12" id="wine_order_winetype">Глинтвейн на красном вине</div>
						<div class="wine_order_price inl_m"><span id="wineprice">XXX</span> <span class="wineunits">грн</span></div>
						<div class="wine_order_x inl_m">&times;</div>
						<div class="wine_order_qty inl_m"><span id="wineqty">X</span> <span class="wineunits">шт</span></div>
						<div class="wine_order_total inl_m winefont12 only_desktop"><span id="winetotal_name">Итого:</span> <span id="wineordertotal">XXX</span> <span class="wineunits">грн</span></div>
						
						<div class="mobile_wine_total only_mobile">Итого: <span id="wineordertotal_mob">XXX</span> <span class="wineunits">грн</span></div>
						
					</div>
					
					<div class="wine_order_inputs">
						<div class="winp"><input id="name" class="inp wineinp" type="text" placeholder="Имя" onkeyup="checkF()"></div>
						<div class="winp"><input id="phone" class="inp wineinp" type="text" placeholder="Номер телефона" onkeyup="checkF()"></div>
						<div class="winp"><input  id="mail" class="inp wineinp" type="text" placeholder="Email" onkeyup="checkF()"></div>
					
						<div class="wsubmt"><div class="wine_button_hover"></div><div class="wine_order_submit" onclick="buywine()">OK</div></div>
						<div class="wine_order_anno">Все поля обязательны для заполнения</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="winetop"><a target="_blank" href="/"><img src="http://gifamin.com/images/svg/logo.svg" alt="" class="winelogo"></a></div>
	
	<section class="winestart">
		<div class="wine_scrolldown" id="todown">
			<div class="inscroll animated fadeInDown infinite"></div>
		</div>
		<div class="instart">
		<div class="tris"></div>	
			<!-- <div class="fullvideo">
				<div class="screen" id="tv"></div>
			</div> -->
			<div class="instart_text">
				<div class="winehello_pic"><img src="images/wine/hero.png" alt="" class="imgresp"></div>
				<div class="winehello_main">Абалденный</div>
				<div class="winehello_add">глинтвейн</div>
			</div>
			
		</div>
		
	</section>
	
	
	
	
	<section class="sect_block" id="nextme">
		
		
		<div class="sec_inner only_desktop">
			<div class="wine_recs">
				<div class="one_wine"><img id="winepic" src="images/wine/wine.jpg" alt="" class="imgresp"></div>
				<div class="one_wine">
					<div class="one_wine_title" id="titlewine">Красное или белое</div>
					<div class="wine_comps" id="aboutwine">
						Выбирать вам!  Главный козырь вин La Vieille Ferme — абсолютная натуральность. Они производятся по всем законам биодинамики, а методы возделывания винограда исключительно органические. Выберите красное La Vieille Ferme за умопомрачительный аромат черной смородины, нотки спелой вишни и специй. А белое сухое подарит глинтвейну аромат груши, спелого персика, лесного ореха и яблока. Вина Ля Вьей Ферм превосходно проявляют себя в глинтвейне. 
					</div>
				</div>
			</div>		
		</div>
		
		
		
		<div class="sec_inner only_mobile">
			<div class="sec_title" id="insert_titlewine"></div>
			<div class="wine_comps" id="insert_aboutwine"></div>
			<div class="one_wine"><img id="insert_winepic" src="" alt="" class="imgresp"></div>		
		</div>
	
	
	
	</section>
	
	
	
	<section class="only_mobile" style="margin-bottom: 40px;"><div class="herbs"><img src="images/wine/herbs.jpg" alt="" class="imgresp"></div></section>
	
	
	

	
	
	<section class="sect_block">
		<div class="sec_title">Специи «Палитра вкуса»</div>
		<div class="sec_inner">
			<div class="spices_desc">Отобраны вручную — именно те, которые сочетаются с французским сухим лучшим образом — ароматный анис, соцветия гвоздики, имбирь, палочки корицы, сладкий изюм, кардамон….</div> 
			<div id="spipic"><img src="images/wine/spx1.jpg" alt="" class="imgresp"></div>
			
		</div>
	</section>
	
	
	<section><div class="herbs"><img src="images/wine/herbs.jpg" alt="" class="imgresp"></div></section>
	
	
	
	<section class="sect_block">
		<!-- <div class="sec_title">Recipe</div> -->
		<div class="sec_inner">
			<div class="wine_recs">
				<div class="one_recipe">
					<div class="rec_title">Ароматный красный</div>
					<div class="rec_comps">
						<ul class="wineul">
							<li>Красное вино «La Vieille Ferme», 750 мл</li>
							<li>Апельсин очистить от кожуры и нарезать<br>полукольцами</li>
							<li>Микс специй: 0,5 пакета</li>
							<li>Сахар: 50-100 граммов</li>
							<li>Вода: 200 мл</li>
						</ul>
					</div>
				</div>
				<div class="one_recipe">
					<div class="rec_title">Витаминный белый</div>
					<div class="rec_comps">
						<ul class="wineul">
							<li>Белое вино «La Vieille Ferme», 750 мл</li>
							<li>Апельсин очистить от кожуры и нарезать<br>полукольцами</li>
							<li>Микс специй: 0,5 пакета</li>
							<li>Столовая ложка мёда</li>
							<li>Вода: 200 мл</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
	
	<section class="newplayer" style="position: relative; width: 100%; height: 100%;">
		<div class="fullvideo">
			<div class="wine_play"></div>
			<div id="player" class="screen"></div>
		</div>
	</section>
	
	
	<!--
	<section class="videosection">
		<div class="videobottom"></div>
		<div class="videotop"></div> 
			<div class="fullvideo">
				<div class="wine_play"></div>
				<div class="screen" id="tv"></div>
			</div>
	</section>
	-->
	<!-- <section class="only_mobile" style="margin:40px 0 10px 0;">
		<div id="spipic"><img src="images/wine/bigx.jpg" alt="" class="imgresp"></div>
	</section> -->
	
	
	<section class="sect_block winebuy">
		<div class="sec_title winebuy">Время сделать выбор</div>
		<div class="sec_inner">
		 	
		 	<div class="wineselectorbox">	
				<div class="wine_selector">
				 <div class="redwhite">
					 <div class="choose_bottle redbottle selectedbottle" attr-bottle="red"><div class="overbottle"></div></div>
					 <div class="winen">Глинтвейн<br>на красном вине</div>
				 </div>
				 <div class="redwhite">
					 <div class="choose_bottle whitebottle graybottle" attr-bottle="white"><div class="overbottle hidechoose"></div></div>
					 <div class="winen">Глинтвейн<br>на белом вине</div>
				 </div>
				</div>
				
				<div class="wine_qty">
					
					<div class="wine_qty_text">Укажите количество наборов. Стоимость одного набора составляет <span id="winepricestart">xxxx</span> грн</div>
					
					<div class="wine_qty_sel">
						<div class="selboxwine h50 mod">
						<select name="" id="winewine" onchange="winecalc();">
						<?php
							for($i=1;$i<=20;$i++){
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
						?>
						</select>
						</div>
					</div>
					
					
					
				</div>
				
				
				<div class="wine_total">Итого: <span id="winetotalstart">XXX</span> грн</div>
				<!-- <div class="wine_details">details</div> -->
				<div class="wine_buy" onclick="openWinePop();">Заказать</div>
			
			<div class="wine_bottom_text">
				В набор для глинтвейна входят: бутылка вина «La Vieille Ferme» (красного или белого), микс специй «Палитра вкуса», апельсин, бумажный пакет.
				<br><br>
				Стоимость указана без учета доставки. Доставка составляет 70 грн.<br>При заказе свыше 1500 грн — доставка бесплатна.
			</div>
			
			</div>	
		
		
		
		</div>
	</section>
	
	<div class="wine_footer">
		<div class="only_mobile" style="font-size: 14px; text-align: center; margin-bottom: 30px;"><span class="binct-phone-number-1">+38 (098) 592 38 08</span></div>
		<div class="wf_l"><a href="/" target="_blank" style="color:#5B143A; text-decoration: none;">На главную</a></div>
		<div class="wf_r"><span class="binct-phone-number-1">+38 (098) 592 38 08</span></div>
	</div>
	
	<div id="client" clientdata="<?php echo $ip?>"></div>
	
	<script>
	wineprice = 450;
	ytbv = 'p8bDO4d8sSg';
	</script>
	
	<script src="js/wine.js"></script>
	
	
	
</body>
</html>