<?php
	$base = 'http://'.$_SERVER['SERVER_NAME'].'/';
	$rand = rand(1000,12222222);
?>
<!DOCTYPE html>
<html lang="en" style="background:#fff;">
<head>
	<meta name="description" content="Мегазуб — подставка для канцелярии.">
	<meta name="keywords" content="Мегазуб, подставка, зуб, купить, киев, подарочный, бокс, box, для, канцелярия">
	<link rel="canonical" href="<?php echo $base;?>">
	<base href="<?php echo $base;?>">
	<meta charset="UTF-8">
	<title>Мегазуб | Gifamin</title>
	<link rel="stylesheet" href="css/main.min.css?v=<?php echo $rand?>">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script src="js/js.js"></script>
	<script src="js/3d.js"></script>
	
			<meta property="og:title" content="GIFAMIN" />
			<meta property="og:url" content="<?php echo $base;?>/specials/megatooth" />
			<meta property="og:image" content="<?php echo $base;?>/images/tooth/fbview.png" />
			<meta property="og:description" content="Подарочный бокс «Мегазуб»" />
			<meta property="fb:app_id" content="1060743084027750" />
	
<?php include('php/static/gtag_head.php')?>
</head>
<body style="background:#fff;">
<?php include('php/static/gtag_body.php')?>	






<div class="tooth_top">
		<div class="tooth_top_center">
			<div class="tooth_main_logo"><a href="/">
				<img src="images/tooth/logo_t.svg" alt="" style="width: 140px;" height="auto">
			</a></div>
		</div>
		
	</div>
<div class="tooth_hello">
		<div class="tooth_inhello">
			<div class="tooth_hero_pic"></div>
			<div class="tooth_inhello_middle">
				<div class="tooth_main_hello">«Мегазуб»</div>
				<div class="tooth_other_hello">Подарочный бокс</div>
			</div>
			
		</div>
			<div id="todownx">
				<div class="tooth_inscroll animated fadeInDown infinite"></div>
			</div>
	</div>


<div class="tooth_post_diag_color">
	<div class="in_diag_down transformme"></div>
</div>
	
	<div class="tooth_section b_p_50" style="background: linear-gradient(#fafbfc, #ffffff 5%,  #ffffff 85%,  #fafbfc);">
		<div class="tooth_headings">
				<h1>Что это?</h1>
				<div class="corp_text" id="what_is">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae magni recusandae aut porro vero explicabo quisquam vitae quibusdam sit natus ipsam tenetur, architecto sunt minima accusamus pariatur dolore, maxime error.
				</div>	
			</div>
	
	<!-- <div class="custom_play">play</div>
	<div class="custom_stop">stop</div> -->
	<div class="dpic">		
		<div class="threesixty product1">
		    <div class="spinner">
		        <span>0%</span>
		    </div>
		    <ol class="threesixty_images"></ol>
			</div> 
	</div>
	
	
	<div class="tooth_3d_control" style="overflow: hidden;">
		<div class="abs_control">
			<div class="incontrol"><img src="images/tooth/prev.svg" alt="" class="animated infinite fadeOutLeft"></div>
			<div class="incontrol"><img src="images/tooth/middle360.svg" alt=""></div>
			<div class="incontrol"><img src="images/tooth/next.svg" alt="" class="animated infinite fadeOutRight"></div>
		</div>
	</div>
    <div class="rotate_anno">Нажми на зуб и крути</div>

</div>



<div class="tooth_post_diag">
	<div class="in_diag_up transformme"></div>
</div>




			
	
	
		<div class="tooth_section b_p_50">
		<div class="tooth_headings">
				<h1>«Анестезия»</h1>
				<div class="corp_text" id="anes">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae magni recusandae aut porro vero explicabo quisquam vitae quibusdam sit natus ipsam tenetur, architecto sunt minima accusamus pariatur dolore, maxime error.
				</div>	
		</div>
	     <div class="tooth_drinks">
					<div class="tooth_one_drink">
						<div class="mobile_hide_rotate"></div>
						<div class="tooth_drink_pic">
							<!--<img src="images/tooth/chivas.png" alt="" class="flexpic">-->
							<div class="threesixty alco_1">
    <div class="spinner">
        <span>0%</span>
    </div>
    <ol class="threesixty_images"></ol>
	</div>
						</div>
						<div class="tooth_drink_anno"></div>
					</div>
					
					<div class="tooth_one_drink">
						<div class="tooth_drink_pic">
							<div class="threesixty alco_2">
    <div class="spinner">
        <span>0%</span>
    </div>
    <ol class="threesixty_images"></ol>
	</div>
						</div>
						<div class="tooth_drink_anno"></div>
					</div>
					
					<div class="tooth_one_drink">
						<div class="tooth_drink_pic">
							<div class="threesixty alco_3">
    <div class="spinner">
        <span>0%</span>
    </div>
    <ol class="threesixty_images"></ol>
	</div>
						</div>
						<div class="tooth_drink_anno"></div>
					</div>
					
					<div class="tooth_one_drink">
						<div class="tooth_drink_pic">
							<div class="threesixty alco_4">
    <div class="spinner">
        <span>0%</span>
    </div>
    <ol class="threesixty_images"></ol>
	</div>
						</div>
						<div class="tooth_drink_anno"></div>
					</div>
		 </div>
	    </div>		


<div class="tooth_post_diag">
	<div class="in_diag_down transformme"></div>
</div>
	
	<div class="tooth_section" style="background: linear-gradient(#fafbfc, #ffffff 45%,   #ffffff);">
		
			<div class="tooth_headings">
				<h1>Упаковка</h1>
				<div class="tooth_text" id="bigpack">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae magni recusandae aut porro vero explicabo quisquam vitae quibusdam sit natus ipsam tenetur, architecto sunt minima accusamus pariatur dolore, maxime error.
				</div>	
			</div>
			<div class="tooth_full_box">
				<img src="images/tooth/blue-min.png" alt="" class="simplebox">
				<!-- <div class="tooth_belt">
					<div class="tooth_overlogo"></div>
					<div class="tooth_logo"></div>
					<div class="tooth_belt_a"></div>
				</div> -->
			</div>
		   
		   <div class="tooth_box_data">
			   <!--<div class="corp_box_title">Подарочная коробка</div>-->
			   <div class="tooth_box_all_parts">
			    <div class="tooth_box_part inl_t">
				    Презентабельная коробка Gifamin имеет необычную форму, выполнена из дизайнерского картона усиленной прочности и декорирована яркой бумажной лентой.
                <!-- <div style="margin-top: 20px;"><a class="gotobox" href="">Подробнее</a></div> -->
			    </div>
			    <div class="tooth_box_part inl_t">
				     <div class="tooth_box_part_x inl_t">
					     <div class="tooth_micro">Длина: 360 мм</div>
					     <div class="tooth_micro">Ширина: 240 мм</div>
				     </div>
				     <div class="tooth_box_part_x inl_t">
					     <div class="tooth_micro">Толщина стенки: 2 мм</div>
					     <div class="tooth_micro">Высота: 120 мм</div>
					 </div>
					 
			    </div>
			   </div> 
		   </div>
	</div>
	
	
	
	<div class="tooth_section toothbuy">
		<div class="tooth_buy_section">
			
			<div class="tooth_headings" style="color:#fff; padding-top: 10px;">
				<h1 style="color:#369FFC;">Купить</h1>
				<div class="tooth_text">
			Курьерская доставка по Киеву составляет 70 грн.<br>
			По Украине — по тарифам «Новой почты» 
				</div>	
			</div>
				<!-- <div class="tooth_icon"></div>
				<div class="tooth_plus">+</div> -->
				<div class="tooth_sel">
					<div class="tooth-select-style">
  <select id="change_tooth">
    <option id="only_tooth" value="" attr-name=""></option>
    <option id="p_metka" value="" attr-name=""></option>
    <option id="p_vodka" value="" attr-name=""></option>
    <option id="p_tequila" value="" attr-name=""></option>
    <option id="p_rum" value="" attr-name=""></option>
  </select>
</div>
				</div>
				
				<div class="tooth_totalbox">
					<div class="tooth_total_x">Стоимость: <span id="tooth_tot_x"></span> <span id="total_cur_x">ГРН</span></div>
				</div>
				
				<div class="tooth_client_data_all">
					<div class="tooth_thanx off">
						<div class="tooth_inthanx">
							<div class="tooth_all_ok ">Спасибо! Скоро мы свяжемся с вами.</div>
						</div>
					</div>
					<div class="tooth_client_data">
						<div><input class="tooth_fieldx" type="text" id="c_name" placeholder="Ваше имя" onkeyup="checkf()"></div>
						<div><input class="tooth_fieldx" type="text" id="c_phone" placeholder="Номер телефона" onkeyup="checkf()"></div>
					</div>
				</div>
				
				<div class="tooth_action">
					<div class="tooth_disable"></div>
					<div class="tooth_btn tooth_btn_dis" onclick="buytooth()">Купить мегазуб</div>
				</div>
				
				<!-- <div class="tooth_total">Total: <span id="tooth_tot">599</span> UAH</div> -->
				
			
		</div>
	</div>
	
	
	
	
	
	
	<div class="tooth_footer">
		<ul>
			
			<li><a href="/">На главную</a></li>
			<li><a href="/">Все наборы</a></li>
			<li><a href="/contacts">Контакты</a></li>
			<li><a href="/"><img src="images/tooth/logo_b.svg" alt="" style="width: 100px; height: auto; padding-top: 1px;"></a></li>
			
		</ul>
	</div>
	
	
	
	
<script>
	
	
	alltexts = [
	'Мечта каждого стоматолога – здоровый, крепкий зуб-холдер, без кариеса и зубного налета – Голливуд в бетоне, или винир на века! Крепкие и могучие корни гарантируют устойчивость, Мегазуб надежно сохранит все мелкие принадлежности, создающие хаос на рабочем столе.',
	'Не банальный лидокаин, а проверенный столетиями исцеляющий напиток крепостью от 38° (Бренди Vecchia Romagna Etiсhetta Nera) до 42° (ром Plantation Barbados Cognac Ferrand). Не превышайте дозировку, не забывайте истины Минздрава, и тогда отменное здоровье и крепкие нервы стоматологу гарантированы!',
	'Презентабельная, крепкая, под стать Мегазубу и анестетику – этому «крылатому боксу» обязательно найдется применение в будущем. Коробка заполнена экологически чистой древесной шерстью.'
	];
	
	$('#what_is').html(alltexts[0]);
	$('#anes').html(alltexts[1]);
	$('#bigpack').html(alltexts[2]);
	
	
	
	$('#todownx').click(function(){
	var w = $(window).height()+100;
	$('html,body').animate({
            scrollTop: w
        	}, 500);
	});
	
	
	
	
	//selected = mega['drinkname'];
	
	drinks = {"drinks":[
	{"drinkname":"Мегазуб без анестезии","price":499,"desc":"none"},
	{"drinkname":"Vecchia Romagna","price":999,"desc":"Бренди, 0.7L"},
	{"drinkname":"Sterling","price":999,"desc":"Водка, 0.75L"},
	{"drinkname":"Espolon Reposado","price":999,"desc":"Текила, 0.75L"},
	{"drinkname":"Plantation Barbados","price":999,"desc":"Ром, 0.7L"}
	]
	};
	
	
	mega = drinks['drinks'][0];
	
	metka = drinks['drinks'][1];
	vodka = drinks['drinks'][2];
	tequila = drinks['drinks'][3];
	rum = drinks['drinks'][4];
	
	
	$('.tooth_drink_anno:eq(0)').html(tequila['drinkname']+'<br><span class="minianno">'+tequila['desc']+'</span>');
	$('.tooth_drink_anno:eq(1)').html(metka['drinkname']+'<br><span class="minianno">'+metka['desc']+'</span>');
	$('.tooth_drink_anno:eq(2)').html(vodka['drinkname']+'<br><span class="minianno">'+vodka['desc']+'</span>');
	$('.tooth_drink_anno:eq(3)').html(rum['drinkname']+'<br><span class="minianno">'+rum['desc']+'</span>');
	
	
	$('#only_tooth').html(mega['drinkname']).attr({'value': mega['price'],'attr-name':mega['drinkname']});
	
	
	$('#p_metka').html('Мегазуб и '+metka['drinkname']).attr({'value': metka['price'], 'attr-name':'Мегазуб и '+metka['drinkname']});
	
	$('#p_vodka').html('Мегазуб и '+vodka['drinkname']).attr({'value': vodka['price'], 'attr-name':'Мегазуб и '+vodka['drinkname']});
	
	$('#p_tequila').html('Мегазуб и '+tequila['drinkname']).attr({'value': tequila['price'],'attr-name':'Мегазуб и '+tequila['drinkname']});
	
	$('#p_rum').html('Мегазуб и '+rum['drinkname']).attr({'value': rum['price'],'attr-name':'Мегазуб и '+rum['drinkname']});
	
	
	option_name = mega['drinkname'];
	option_price = 499;
	$('#tooth_tot_x').html(option_price);
	
	$('#change_tooth').on('change', function() {
		option_name = $('option:selected', this).attr('attr-name');
		option_price = this.value;
		$('#tooth_tot_x').html(option_price);
  	});
	
	//alert(metka['drinkname']);
	
	function checkf(){
		if($('#c_name').val().length < 2 || $('#c_phone').val().length < 2 ){
			$('.tooth_btn').addClass('tooth_btn_dis');
			$('.tooth_disable').css({'display':'block'});
		}else{
			$('.tooth_btn').removeClass('tooth_btn_dis');
			$('.tooth_disable').css({'display':'none'});
		}
	}
	
	
$(document).ready(function () {
    $.getJSON("http://jsonip.com/?callback=?", function (data) {
        clientip = data.ip;
    });
});
	
	function buytooth(){
		clientname = $('#c_name').val();
		clientphone = $('#c_phone').val();
		data = {"bundle":option_name, "price":option_price, "name":clientname, "phone":clientphone, "ip":clientip};
		datax = JSON.stringify(data);
		
	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", "x_tooth_ajax.php?data="+datax, true);
	xhttp.send();
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
	    //document.getElementById("fastorder").innerHTML = xhttp.responseText;
	    if(xhttp.responseText=='tooth'){
		   // alert('thank u');
		   console.log(xhttp.responseText);
		   $('.tooth_thanx').fadeIn(400);
		   $('.tooth_btn').addClass('tooth_btn_dis');
		   $('.tooth_disable').css({'display':'block'}); 
		   setTimeout(function(){
		   $('.tooth_thanx').fadeOut(400);
		   $('.tooth_btn').removeClass('tooth_btn_dis');
		   $('.tooth_disable').css({'display':'none'}); 
		   }, 4000);
	    }else{
		 //alert('oops!');   
	    }
	    
        }
    };

		
		
	}

		
	if($(window).width()>1024){
	setInterval(function(){
	setTimeout(function(){
		$('.in_diag_up').addClass('changediag');
	}, 2000);
	setTimeout(function(){
		$('.in_diag_up').removeClass('changediag');
	}, 4000);}
	, 6000);
	
	setInterval(function(){
	setTimeout(function(){
		$('.in_diag_down').addClass('changediag');
	}, 2000);
	setTimeout(function(){
		$('.in_diag_down').removeClass('changediag');
	}, 6000);}
	, 8000);
	}else{
		
	}
	
	
	

	window.onload = init;

var threedme;
var alco_1;
var alco_2;
var alco_3;
var alco_4;
function init(){

    threedme = $('.product1').ThreeSixty({
        totalFrames: 48, // Total no. of image you have for 360 slider
        endFrame: 48, // end frame for the auto spin animation
        currentFrame: 1, // This the start frame for auto spin
        imgList: '.threesixty_images', // selector for image list
        progress: '.spinner', // selector to show the loading progress
        imagePath:'images/tooth/graphics/3dxa/', // path of the image assets
        filePrefix: '', // file prefix if any
        ext: '.jpg', // extention for the assets
        //playSpeed:100,
        //framerate:60,
        //height: 500,
        //width: 347,
        navigation: false,
        responsive: true
    });
    
    
    <?php
	    $paths = array('esp','metka','ster','rum');
	    for($i=1;$i<5;$i++){
		    //ster 
		    
		    echo "
		    alco_".$i." = $('.alco_".$i."').ThreeSixty({
        totalFrames: 48, // Total no. of image you have for 360 slider
        endFrame: 48, // end frame for the auto spin animation
        currentFrame: 1, // This the start frame for auto spin
        imgList: '.threesixty_images', // selector for image list
        progress: '.spinner', // selector to show the loading progress
        imagePath:'images/tooth/graphics/".$paths[$i-1]."/', // path of the image assets
        filePrefix: '', // file prefix if any
        ext: '.jpg', // extention for the assets
        playSpeed:60,
        framerate:60,
        //height: 500,
        //width: 347,
        navigation: false,
        responsive: true
    });
		    ";
	    
	    
	    echo "alco_".$i.".play();";
	    
	    
	    echo "
	    
	    $( '.alco_".$i."' )
  .mouseenter(function() {
    alco_".$i.".stop();
  })
  .mouseleave(function() {
    alco_".$i.".play();
  });
	    
	    ";
	    
	    }
    ?>
    
    

}


</script>

</body>
</html>