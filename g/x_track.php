<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gifamin | xTracker</title>
	<script   src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<body>

<style>
	.head{font-family: 'Arial'; color:rgba(0,0,0,0.33);}
	ul{margin: 0; padding: 0; font-family: 'Arial';}
	li{list-style-type: none; border-bottom: 1px solid #eee; padding-bottom: 10px; margin-bottom: 10px;}
	.doc{font-weight: bold;}
	.sdate{font-size: 13px; color:rgba(0,0,0,0.34); margin: 5px 0;}
	.status{font-size: 14px; color:rgba(0,0,0,0.77);}
	.user{font-size: 11px; font-weight: bold;}
	.okcolor{color:green!important;}
</style>


<h1 class="head">NP API Tracker</h1>
<div>
	<ul id="res"></ul>
</div>



<?php
$take_file = $_GET['tracking'];
$input_doc = 'x_track/'.$take_file.'.xls';
//$input_doc = 'x_track/avtek_track.xls';


include_once 'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';
function getXLS($xls, $sheet){
    $objPHPExcel = PHPExcel_IOFactory::load($xls);
    $objPHPExcel->setActiveSheetIndex($sheet);
    $aSheet = $objPHPExcel->getActiveSheet();
	$array = array();
    foreach($aSheet->getRowIterator() as $row){
      $cellIterator = $row->getCellIterator();
      
      $item = array();
      $cellIterator->setIterateOnlyExistingCells(false);
      foreach($cellIterator as $cell){
        array_push($item, $cell->getCalculatedValue());
      }
      array_push($array, $item);
    }
    return $array;
}	




$data = '';
parseTrack($input_doc, 0);





  function parseTrack($doc, $x){
	  global $data;
	 $xlsData = getXLS($doc, $x); //извлеаем данные из XLS
	  foreach(array_slice($xlsData, 1) as $one){
	  	if(!empty($one[0])){
		   $doc_num = $one[0];
		   $doc_phone = $one[1];
		   $doc_user = $one[2];
		   $data .='{"number":"'.$doc_num.'","phone":"'.$doc_phone.'","user":"'.$doc_user.'"},';
		    $data .= "\n";
		 
		 
		 } 
	  }
 
     
  } 
  
  $data = substr($data, 0, -2);
  //$data = json_decode($data);
  //print_r($data);
	
	
?>

	

<script>
api = 'fa8c8b1bf8cfe8ac64166ab77b9a149f';
url = 'https://api.novaposhta.ua/v2.0/json/';	


docs = {
	'alldocs':[
		<?php echo $data?>
		]
};



for(i=0;i<docs["alldocs"].length;i++){
	//console.log(docs["alldocs"][i]["number"]);
	trackme(docs["alldocs"][i]["number"], docs["alldocs"][i]["phone"], docs["alldocs"][i]["user"]);
}





function trackme(n, p, u){	
	 var xhttp = new XMLHttpRequest();
	 xhttp.open("POST", url, true);
	 xhttp.setRequestHeader('Content-Type', 'application/json');
	 xhttp.send(JSON.stringify(

{
    "apiKey": "fa8c8b1bf8cfe8ac64166ab77b9a149f",
    "modelName": "TrackingDocument",
    "calledMethod": "getStatusDocuments",
    "methodProperties": {
        "Documents": [
            {
                "DocumentNumber": n,
                "Phone": p
            }
        ]
    }
}
)
     );
	 
	 xhttp.onreadystatechange = function() {
	 if (this.readyState == 4) {
	    	
	    	
	    	
	    	answer = JSON.parse(this.responseText);
	    	console.log(answer);
	    	status = answer['data'][0]['Status'];
	    	date = answer['data'][0]['ScheduledDeliveryDate'];
	    	
	    	if(status == 'Відправлення отримано'){
		    	okcolor='okcolor';
	    	}else{
		    	okcolor='';
	    	}
	    	
	    	//alert('Number: '+x+' STATUS: '+status);
	    	$("#res").append("<li><div class='doc'>"+n+"</div><div class='user'>"+u+"</div><div class='sdate'>Прогнозируемая дата доставки: "+date+"</div>  <div class='status "+okcolor+"'> "+status+"</div></li>");
         
         }
      }
}


//trackme('59000306995389','380503332787');
</script>


</body>
</html>