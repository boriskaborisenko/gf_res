<?php

include('connect.php');



$item_per_page = 3;

//$page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

$page_number = filter_var($_GET["page"]);

if(!is_numeric($page_number)){
    header('HTTP/1.1 500 Invalid page number!');
    exit();
}

$position = (($page_number-1) * $item_per_page);


$tag = $_GET['tag'];
//$tag = 'start';

if(is_numeric($tag)){		
		
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND start = 1
		AND cat_id=?
		ORDER BY priority ASC LIMIT ?, ?
		
		");	
		$results->bind_param("ddd", $tag, $position, $item_per_page); 

}



if(!is_numeric($tag)){
	    
	if($tag == "start"){
		
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND start=1
		ORDER BY priority ASC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
		
	}
	
	if($tag == "popular"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND popular=1
		ORDER BY id ASC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	
	
	//TAGS
	
	if($tag == "tag_a"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_a=1
		ORDER BY id ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
    if($tag == "tag_b"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_b=1
		ORDER BY priority ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "tag_c"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_c=1
		ORDER BY id ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "tag_d"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_d=1
		ORDER BY id ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "tag_e"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND price_sale!=0
		ORDER BY id ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "tag_event"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_event=1
		ORDER BY priority ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	//TAGS
	
	
	
	if($tag == "accessory"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND accessory=1
		ORDER BY priority ASC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "allfeed"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		ORDER BY priority ASC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
		    

}



$results->execute(); //Execute prepared Query
$results->bind_result($pid, $p_name, $p_desc, $p_images, $price_out, $price_sale, $pack_type, $priority, $author, $cat_id, $cat_name); //bind variables to prepared statement





//output results from database



$n = 0;
while($results->fetch()){ //fetch values
	
	
	
	 ?>
	 <div class="box_mobile">
	  <div class="one_prod">
		 <div class="mob_hero" style="background-image: url(<?php echo 'http://gifamin.com/images/products_pics/'.$pid.'/1.jpg';?>);" onclick="oneProd(<?php echo $pid?>);">
			 <?php if($price_sale>0){
				 //if($discount>0){
				  $oldprice = number_format($price_out, 0, '.', ' ');
				  $newprice = number_format($price_sale, 0, '.', ' ');
				  $discount = number_format( (100-(100*$price_sale/$price_out)), 0, '.', ' ');
				  echo "<div class='discount_bage'>
				  <div class='bage_nums'>
				  	<div class='bage_old_price inl_m'>$price_out <span>грн</span></div>
				  	<div class='bage_dis inl_m'>-$discount<span>%</span></div>
				  </div>
				  </div>";
				  //}
			  }?>
		 </div>
		 
		
		 
		 <div class="prod_data">
							<div class="inbox bt">
								<div class="prod_title inl_m" onclick="oneProd(<?php echo $pid?>);">
									<div class="elips"></div>
									<?php echo 'Набор '.html_entity_decode($p_name);?>
								</div>
								<div class="prod_price inl_m">
									<div class="inprice" onclick="oneProd(<?php echo $pid?>);">
										<?php 
											if($price_sale>0){
												$showprice = $price_sale;
											}else{
												$showprice = $price_out;
											}
											
											echo number_format($showprice, 0, '.', ' ');
											
										?> <span>грн.</span></div>
								</div>
							</div>
						</div>
						
		 
	  </div>
	 </div>
	 
	<?php
	}

?>

