<div class="corporate_mobile">
	<div class="mobile_inner_cc">



<h3 style="text-transform: uppercase;">Корпоративным клиентам</h3>

 


<h3 style="margin-bottom: 10px;">Индивидуальные наборы</h3>
<p>
Кроме наборов, представленных в каталоге, есть возможность собрать особенные наборы по вашим пожеланиям. Мы найдем именно тот алкогольный напиток, который хочет видеть к празднику наш клиент, и подберем грамотное сочетание к нему. 
</p>

<h3 style="margin-bottom: 10px;">Брендирование</h3>
<p>
Размещение логотипа вашей компании на подарочном наборе. Брендирование — это отличный вариант создать имидж компании, обзавестись новыми клиентами и партнерами или поздравить существующих.
</p>

<h3 style="margin-bottom: 10px;">Индивидуальное поздравление</h3>
<p>
Каждый подарок удивит вас «легендой» – творческим описанием набора. Мы с удовольствием напишем персональное поздравление, обращаясь к получателю по имени, и с пожеланиями, которые вы сообщите нам заранее.
</p>

<hr style="border: 0;
    height: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    border-bottom: 1px solid rgba(255, 255, 255, 0.3); margin: 40px 0;">
    
    <!--MOBILE CAL-->
    
    
    
<div class="calendar inl_m" style="width: 120px; height: 120px; background: url('../images/calendar3.png') center center no-repeat; background-size: cover; position: relative;">
	<div class="cal_month" style="text-align: center; background: rgba(255,0,255,0.0); font-weight: bold; text-transform: uppercase; margin-top: 10px; font-size: 11px; color:#fff;" id="calmonth"></div>
	<div class="cal_date" style="text-align: center; font-size: 60px; font-weight: bold; margin-top: 32px;" id="caldate"></div>
</div>

<div id="caltext" class="calendar_text inl_m" style="width:500px; margin-left: 20px;">
	<h3 style="margin-top: 0!important;">Календарь поздравлений партнеров</h3>
Грамотное решение для всех, кто регулярно ломает голову вопросом, что подарить партнеру и как сделать это вовремя. 
Вы предоставляете необходимую информацию об именинниках (на 3, 6, 9 или 12 месяцев), а дальнейшие хлопоты мы берем на себя. Далее мы информируем вас о проделанной работе.
</div>




    
    <!--MOBILE CAL-->
    
<hr style="border: 0;
    height: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    border-bottom: 1px solid rgba(255, 255, 255, 0.3); margin: 40px 0;">    


    
<h4>Контактная информация</h4>
<p>
	+38 (093) 221-28-08<br>
	info@gifamin.com
</p>

<br><br>
<p>
<!-- <h4>Реквізити Продавця:</h4> -->
<b>Товариство з обмеженою відповідальністю «ГІФАМІН»</b><br>
Юридична адреса: 03028, м. Київ, Голосіївський Район,<br>
вулиця Добрий  Шлях, будинок  5, квартира 11.<br>
Код ЄДРПОУ 40659768<br>
Банківські реквізити:п/р №26000053161475, <br>
в ПАТ «КБ «Приватбанк», МФО банку 321842<br>
Директор: Михайленко О. Г.
</p>


</div>
</div>