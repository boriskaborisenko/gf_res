<div class="giftcards_hello">
	<div class="giftcards_hello_inner">
		<div class="giftcards_mobile_hello_text">
			
			
			        <div class="onlyMob"></div>
					<div class="giftcards_title">Подарочный сертификат</div>
					<div class="giftcards_text">Идеальный способ поздравить человека и сохранить право выбора за ним</div>
		</div>
	</div>
</div>
		
		
<div class="giftcards_block">
	<div class="inner">
		
		<!-- <div class="giftcards_header">Номиналы</div> -->
		
		<div class="all_gifts">
			
			<div class="one_gift_card">
				<div class="giftcard_face gc_100"></div>
			</div>
			
			<div class="one_gift_card">
				<div class="giftcard_face gc_200"></div>
			</div>
			
			<div class="one_gift_card">
				<div class="giftcard_face gc_500"></div>
			</div>
			
			<div class="one_gift_card">
				<div class="giftcard_face gc_1000"></div>
			</div>
			
		</div>
	</div>
</div>

<div class="giftcards_block">
	<div class="inner g_small_anno">
		<div class="giftcards_rules_title">
			Правила использования сертификата:
		</div>
		<div class="giftcards_rules">
			<ul>
				<li>Подарочный сертификат (далее «сертификат») является сертификатом на предъявителя (не является именным), и любое физическое лицо, предъявившее сертификат, может приобрести с его помощью товар, предлагаемый компанией Gifamin. 
				</li>
				
				<li>
				В случае утраты сертификата, в том числе хищения, сертификат не может быть восстановлен, а денежные средства не могут быть возвращены, в связи с отсутствием персонификации лица, владевшего сертификатом до утраты.
				</li>
				
				<li>
				 При оплате заказа с помощью сертификата обязательно укажите это на этапе оформления и сообщайте менеджеру при подтверждении заказа. Сертификат должен быть предъявлен и передан сотруднику компании при оплате товара. 
				</li>
				
				<li>
				Срок действия Сертификата ограничен и составляет 6 месяцев с даты выдачи.
				</li>
				
				<li>
				С использованием сертификата возможна единственная покупка. При совершении покупки номинал сертификата списывается целиком. В случае, если стоимость покупки меньше номинала сертификата, разница между стоимостью не возвращается. 
				</li>
				
				<li>
				При возврате товара, купленного с использованием сертификата, возврат средств осуществляется на новый сертификат аналогичного номинала.
				</li>
				
				<li>
				В случае, если стоимость покупки превышает номинал, разница между стоимостью товара и номиналом сертификата оплачивается наличными денежными средствами.
				</li>
				
				<li>
				Сертификат не может быть использован для получения наличных денежных средств.
				</li>
				
				<li>
				Оплатить товары с помощью сертификата могут только физические лица. Юридические лица не могут частично или полностью оплатить приобретение товаров сертификатами.
				</li>
				
				<li>
				Покупателю следует сохранять чек на покупку сертификата для подтверждения номинала карты в случае повреждения или иных спорных ситуаций. 
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="giftcards_block" style="background: #fff;">
	<div class="inner">
		<div class="giftcards_header">Купить сертификаты</div>
		<div class="cards_amount_anno">Выберите количество</div>
		<div class="buy_card_box">
			
			
			<div class="buy_card_part">
				<div class="card_vars">
					
					
					<?php
						$cards = array(100,200,500,1000);
						foreach($cards as $card){
							?>
							<div class="card_var">
								<div id="id_nom_<?php echo $card?>" class="card_nom inl_m"><?php echo $card?></div>
									<div class="card_amount inl_m">
										<div class="selbox_2">
										<select id="nom_<?php echo $card?>" name="sel_qty" class="amnt" onchange="card_amount(this.id);">
							 <?php
								 for($i=0;$i<11;$i++){
									 echo '<option value="'.$i.'">'.$i.' шт.</option>';
								 }
							 ?>				
							 			</select>
							 			</div>
							 	</div>
							 </div>
						<?php	
						}
					?>
					
					
					
					
					
				</div>
				
				
				<div class="cards_total">Общая стоимость: <span id="total_cards_sum">0 грн</span></div>
				<div class="cards_total_counter">Количество сертификатов: <span id="total_cards">0</span> шт</div>
				
				<div class="buy_cards_data">
					<div class="hover_buy_thanks off">
						<div class="card_thanx">Спасибо.</div>
						<div class="card_thanx_anno">Мы свяжемся с вами в ближайшее время</div>
					</div>
					<div class="hover_buy"></div>
					<div class="cards_buy_form">
						
						<div class="one_inp"><input class="inp mob_inp" type="text" value="" placeholder="Ваше имя" tplaceholder="your_name" id="un" onkeyup="userkeys(this.value, this.id)"></div>
							
							<div class="one_inp"><input class="inp mob_inp" type="text" value="" placeholder="+38 (___) ___-__-__" id="up" onkeyup="userkeys(this.value, this.id)"></div>
							<div class="one_inp"><input class="inp mob_inp" type="text" value="" placeholder="e-mail"  id="um" onkeyup="userkeys(this.value, this.id)"></div>
							<div class="one_inp"><input class="inp mob_inp" type="text" value="" placeholder="Куда доставить и комментарий" tplaceholder="del_addr" id="us" onkeyup="userkeys(this.value, this.id)"></div>
							
							
							<div class="note_card">Все поля обязательны для заполнения</div>
							
							<div class="final_btn">
								<div class="hider" id="final_step"></div>
							    <div class="btn2smob" onclick="buycards()">Купить</div>
							</div>
					
					</div>
				</div>
				
			</div>
			
			
		</div>
	</div>
</div> 



